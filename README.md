# Google Navigation SDK Sample Project

This sample project provides an example of using the Google Navigation SDK in an
Android Studio project.

## Installation

-   Open this sample app in Android Studio.

-   This demo app is compatible with a range of supported Navigation SDK
    versions, as indicated by the name of the containing .zip file in Google
    Drive.

-   A default compatible version number has been supplied in the app-level
    `build.gradle` file under the variable named `navSdkVersion`. Make sure to
    update that variable's value to the version of NavSDK you'd like to test.

-   Update the YOUR_API_KEY value in the local.defaults.properties to your own
    API key from a project that has been authorized to use the Google Navigation
    SDK. This API key must also have access to the Google Places API enabled in
    order to be able to search for places in the sample application.
    See [instructions](https://developers.google.com/maps/documentation/android-sdk/start#get-key)
    for how to get your own key and learn more about
    [Secrets Gradle plugin](https://developers.google.com/maps/documentation/android-sdk/secrets-gradle-plugin)
    to keep the key out of version control systems.

-   [Ensure](https://developers.google.com/maps/documentation/navigation-sdk-android/v2/config_v2#using_maven_recommended)
    that your gcloud credentials have access to the remote repository. This is
    to enable fetching the NavSDK from the Maven artifact repository.

    -   Alternatively, you can remove the artifactregistry plugin and repository
        from the build.gradle files and replace the NavSDK dependency with the
        dependency on the aar file. E.g. `api(name: 'google_navigation', ext:
        'aar')`. The aar is no longer included directly in the sample app and
        you will have to manually add it to the projects libs folder.

-   Build and run the sample application.
