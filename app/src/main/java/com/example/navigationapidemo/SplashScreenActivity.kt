package com.example.navigationapidemo

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageItemInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.Places
import java.util.concurrent.TimeUnit

/** The main activity showing a splash screen and requesting for location permission. */
class SplashScreenActivity : AppCompatActivity() {
  override fun onCreate(bundle: Bundle?) {
    super.onCreate(bundle)
    Places.initialize(applicationContext, getApiKeyFromMetaData())
    setContentView(R.layout.activity_splash_screen)
    val imageView = findViewById<ImageView>(R.id.splash_image)

    Glide.with(this)
      .load("http://services.google.com/fh/files/misc/google_maps_logo_480.png")
      .placeholder(R.drawable.google_maps_logo)
      .fitCenter()
      .into(imageView)

    if (
      ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) !=
      PackageManager.PERMISSION_GRANTED
    ) {
      if (
        ActivityCompat.shouldShowRequestPermissionRationale(this, permission.ACCESS_FINE_LOCATION)
      ) {
        finish()
      } else {
        ActivityCompat.requestPermissions(
          this,
          arrayOf(permission.ACCESS_FINE_LOCATION),
          MY_PERMISSIONS_REQUEST_ACCESS_LOCATION
        )
      }
    } else {
      Handler().postDelayed({ onLocationPermissionGranted() }, SPLASH_SCREEN_DELAY_MILLIS)
    }
  }

  private fun onLocationPermissionGranted() {
    val mainActivity = Intent()
    mainActivity.action = MAIN_ACTIVITY_INTENT_ACTION
    startActivity(mainActivity)
    finish()
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_LOCATION) {
      if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        onLocationPermissionGranted()
      } else {
        finish()
      }
    }
  }

  /**
   * Gets the Google Maps Api Key for the Places API from Metadata.
   *
   * @throws RuntimeException if meta data com.google.android.geo.API_KEY doesn't exist.
   * @return The API key from AndroidManifest.
   */
  private fun getApiKeyFromMetaData(): String {
    return try {
      val packageInfo: PackageItemInfo =
        getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA)
      packageInfo.metaData.getString("com.google.android.geo.API_KEY")!!
    } catch (e: PackageManager.NameNotFoundException) {
      throw RuntimeException("com.google.android.geo.API_KEY not defined in Manifest")
    }
  }

  companion object {
    const val MAIN_ACTIVITY_INTENT_ACTION = "com.example.navigationapidemo.intent.action.MAIN"
    private const val MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 100
    private val SPLASH_SCREEN_DELAY_MILLIS = TimeUnit.SECONDS.toMillis(2)
  }
}
