package com.example.navigationapidemo

import com.google.android.gms.maps.GoogleMap

/**
 * Used in [NavViewActivity.withMapAsync] and [NavFragmentActivity.withMapAsync] to provide
 * asynchronous access to the Google Map.
 */
interface InitializedMapScope {
  val map: GoogleMap
}
