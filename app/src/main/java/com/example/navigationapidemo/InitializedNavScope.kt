package com.example.navigationapidemo

import com.google.android.libraries.navigation.Navigator

/**
 * Used with in [NavViewActivity.withNavigatorAsync] and [NavFragmentActivity.withNavigatorAsync] to
 * provide asynchronous access to the Navigator.
 */
open class InitializedNavScope(val navigator: Navigator)

typealias InitializedNavRunnable = InitializedNavScope.() -> Unit
