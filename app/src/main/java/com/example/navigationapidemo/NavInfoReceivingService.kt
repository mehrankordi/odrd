package com.example.navigationapidemo

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.os.Messenger
import android.os.Process
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.libraries.mapsplatform.turnbyturn.TurnByTurnManager
import com.google.android.libraries.mapsplatform.turnbyturn.model.NavInfo

/**
 * Receives turn-by-turn navigation information forwarded from NavSDK and posts each update to live
 * data, which is then displayed on a separate header in `NavInfoDisplayFragment`. This service may
 * be part of a different process aside from the main process, depending on how you want to
 * structure your app. The service binding will be able to handle interprocess communication to
 * receive nav info messages from the main process.
 */
class NavInfoReceivingService : Service() {
  /** The messenger used by the service to receive nav step updates. */
  private var incomingMessenger: Messenger? = null

  /** Used to read incoming messages. */
  private var turnByTurnManager: TurnByTurnManager? = null

  private inner class IncomingNavStepHandler(looper: Looper) : Handler(looper) {
    override fun handleMessage(msg: Message) {
      if (TurnByTurnManager.MSG_NAV_INFO == msg.what) {
        // Read the nav info from the message data,
        // and post the value (if it exists) to LiveData to be displayed in the nav info header.
        turnByTurnManager?.readNavInfoFromBundle(msg.data).let { navInfo ->
          navInfoMutableLiveData.postValue(navInfo)
        }
      }
    }
  }

  override fun onBind(intent: Intent): IBinder? {
    return incomingMessenger?.binder
  }

  override fun onUnbind(intent: Intent): Boolean {
    navInfoMutableLiveData.postValue(null)
    return super.onUnbind(intent)
  }

  override fun onCreate() {
    turnByTurnManager = TurnByTurnManager.createInstance()
    val thread = HandlerThread("NavInfoReceivingService", Process.THREAD_PRIORITY_DEFAULT)
    thread.start()
    incomingMessenger = Messenger(IncomingNavStepHandler(thread.looper))
  }

  companion object {
    private val navInfoMutableLiveData = MutableLiveData<NavInfo?>()
    val navInfoLiveData: LiveData<NavInfo?>
      get() = navInfoMutableLiveData
  }
}
