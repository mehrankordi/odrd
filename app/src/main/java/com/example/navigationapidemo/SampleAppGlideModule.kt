package com.example.navigationapidemo

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/** An AppGlideModule implementation for Glide's Generated APIs */
@GlideModule class SampleAppGlideModule : AppGlideModule()
