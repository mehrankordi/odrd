package com.example.navigationapidemo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import java.util.Arrays

/** An activity to host AutocompleteSupportFragment from Places SDK. */
class PlacePickerActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_place_picker)
    // Initialize the AutocompleteSupportFragment.
    val autocompleteFragment =
      supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
        as AutocompleteSupportFragment?

    // Specify the types of place data to return.
    autocompleteFragment?.setPlaceFields(
      Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.TYPES)
    )

    // Set up a PlaceSelectionListener to handle the response.
    autocompleteFragment?.setOnPlaceSelectedListener(
      object : PlaceSelectionListener {
        override fun onPlaceSelected(place: Place) {
          setResult(RESULT_OK, Intent().putExtra("PLACE", place))
          finish()
        }

        override fun onError(status: Status) {
          setResult(RESULT_CANCELED, Intent().putExtra("STATUS", status))
          finish()
        }
      }
    )
  }

  companion object {
    fun getPlace(data: Intent): Place {
      return data.getParcelableExtra("PLACE")!!
    }
  }
}
