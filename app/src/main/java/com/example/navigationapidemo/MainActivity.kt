package com.example.navigationapidemo

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

/** Main activity that lets the user choose a demo to launch. */
class MainActivity : AppCompatActivity() {
  public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.main)
    val listAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, DEMOS.keys.toList())
    val listView = findViewById<ListView>(R.id.list_view)
    listView.adapter = listAdapter
    listView.onItemClickListener = OnItemClickListener { parent, view, position, _ ->
      val demoName = parent.getItemAtPosition(position) as String
      startActivity(Intent(view.context, DEMOS[demoName]))
    }
  }

  companion object {
    private val DEMOS =
      mapOf<String, Class<*>>(
        "NavViewActivity" to NavViewActivity::class.java,
        "NavFragmentActivity" to NavFragmentActivity::class.java,
        "SwappingMapAndNavActivity" to SwappingMapAndNavActivity::class.java
      )
  }
}
